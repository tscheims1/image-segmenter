# Image Segmenter

A tool for manual image segmentation. It stores the segmented regions as binary mask. Tested with Windows 10 and Linux Manjaro


## installation

1. download qt5.0
2. import and compile projekt

## user instruction

1. choose a folder with images to segment by pressing the "choose folder" button
2. segment the image by drawing on the screen
3. press the "save" button to save the segmented regions: In the choosen image folder there is a folder named "masks". In this folder are all binary images stored.
4. press "next" button to segment the next images.
