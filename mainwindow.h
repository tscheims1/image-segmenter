#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QDebug>
#include <QMouseEvent>
#include <QFileDialog>
#include <QMessageBox>
#include <QScreen>
#include <cmath>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    void loadImage(QString fileName);
    QString buildImagePath(QString fileName);
    QString getMaskDir();
    void setCurrentImage();
    QStringList images;
    QStringList::const_iterator imagesIt = nullptr;
    QString currentDir;
    QSize maxImageSize;
    QSize originalImageSize;
    bool isErasing = false;
    int currentImageNo = 0;
    int totalImages = 0;
private slots:
    void openDir();
    void nextImage();
    void prevImage();
    void saveImage();
    void clearMask();
    void eraseDrawToggle();
    void jumpTo();

};

#endif // MAINWINDOW_H
