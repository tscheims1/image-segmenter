#include "clickablelabel.h"

ClickableLabel::ClickableLabel(QWidget* parent)
    : QLabel(parent)
{
}
void ClickableLabel::setupPainter()
{
    if(pixmap !=nullptr)
    {
        painter->end();
        delete painter;
        delete pixmap;
    }
    pixmap = new QPixmap(width(),height());
    pixmap->fill(QColor("transparent"));
    painter = new QPainter(pixmap);
    painter->setOpacity(0.7);
    QPen pen;
    pen.setWidth(3);
    painter->setPen(pen);
    setPixmap(*pixmap);
}
void ClickableLabel::setupPainter(QPixmap pm)
{
    if(pixmap !=nullptr)
    {
        painter->end();
        delete painter;
        delete pixmap;
    }

    pixmap = new QPixmap(pm);
    painter = new QPainter(pixmap);
    painter->setOpacity(0.7);
    QPen pen;
    pen.setWidth(3);
    painter->setPen(pen);
    setPixmap(*pixmap);
}

ClickableLabel::~ClickableLabel()
{
    painter->end();
    delete pixmap;
    delete painter;
}
void ClickableLabel::mouseMoveEvent(QMouseEvent* event)
{

    if(isFirstNode)
    {
        path.moveTo(event->x(),event->y());
        isFirstNode = false;
    }
    else
    {
        path.lineTo(event->x(),event->y());
    }

    painter->drawPath(path);
    setPixmap(*pixmap);


}
void ClickableLabel::mouseDoubleClickEvent( QMouseEvent * event)
{
    if(!isErasing)
    {
        QBrush brush(QColor (255, 0, 0));
        painter->fillPath(path,brush);
        setPixmap(*pixmap);
        isFirstNode = true;
        path = QPainterPath();
    }
    else
    {
        QBrush brush(QColor (0, 255, 0));


        painter->fillPath(path,brush);
        QImage toDeleteMap = pixmap->toImage();

        for(int x = 0; x < toDeleteMap.width();x++)
        {
            for(int y = 0; y < toDeleteMap.height();y++)
            {
                if( toDeleteMap.pixel(x,y) != oldMap.pixel(x,y))
                {
                    oldMap.setPixel(x,y,0x00);
                }
            }
        }
        setupPainter(QPixmap::fromImage(oldMap));
        setPixmap(*pixmap);
        isFirstNode = true;
        path = QPainterPath();
    }

}
void ClickableLabel::clearMask()
{
    pixmap->fill(QColor("transparent"));
    setPixmap(*pixmap);
    isFirstNode = true;
    path = QPainterPath();
}
void ClickableLabel::changeDrawMode(bool isErasing)
{
    this->isErasing = isErasing;
    //save old map: delete diff afterwards
    oldMap = pixmap->toImage();
}
bool ClickableLabel::savePixmap(QString fileName,QSize originalMaskSize)
{
    //Save Image as B/W Mask
    QPixmap scaledPixmap =  pixmap->scaled(originalMaskSize);
    QImage image = scaledPixmap.toImage();
    QImage tmpMask(image.width(),image.height(),QImage::Format_Mono);
    for(int x=0; x < image.width();x++)
        for(int y =0; y < image.height();y++)
        {
            if(image.pixel(x,y))
                tmpMask.setPixel(x,y,0x01);
            else
                tmpMask.setPixel(x,y,0x00);
        }
    return tmpMask.save(fileName,0,100);//no compression
}
