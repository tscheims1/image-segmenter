#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)

{
    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->geometry();

    maxImageSize.setWidth(screenGeometry.width()-100);
    maxImageSize.setHeight(screenGeometry.height()-200);

    ui->setupUi(this);
    connect(ui->openDirButton, SIGNAL (released()), this, SLOT (openDir()));
    connect(ui->prevImageButton, SIGNAL (released()), this, SLOT (prevImage()));
    connect(ui->nextImageButton, SIGNAL (released()), this, SLOT (nextImage()));
    connect(ui->saveButton, SIGNAL (released()), this, SLOT (saveImage()));
    connect(ui->clearButton, SIGNAL (released()), this, SLOT (clearMask()));
    connect(ui->eraseDrawButton, SIGNAL (released()), this, SLOT (eraseDrawToggle()));
    connect(ui->jumpToButton, SIGNAL (released()), this, SLOT (jumpTo()));




}
void MainWindow::openDir()
{
    currentDir = QFileDialog::getExistingDirectory(this, tr("Open Image Directory"),
                                                 "/home",
                                                 QFileDialog::DontResolveSymlinks);
    QDir recoredDir(currentDir);
    QStringList filters;
    filters << "*.png" << "*.jpg" <<"*.JPG";
    recoredDir.setNameFilters(filters);
    images = recoredDir.entryList(QDir::NoDotAndDotDot | QDir::Filter::Files);
    if(!images.isEmpty())
    {
        imagesIt = images.constBegin();
        loadImage(*imagesIt);
        currentImageNo = 1;
        totalImages = images.count();
        setCurrentImage();
    }

}
void MainWindow::loadImage(QString fileName)
{
    QImage image(buildImagePath(fileName));
    originalImageSize = QSize(image.width(),image.height());

    int maxWidth = fmin(image.width(),maxImageSize.width());
    int maxHeight = fmin(image.height(),maxImageSize.height());

    //keep image ratio
    float ratioWidth = ((float)maxWidth)/image.width();
    float ratioHeight = ((float)maxHeight)/image.height();

    if(ratioWidth > ratioHeight)
        maxWidth = image.width()*ratioHeight;
    else if(ratioWidth < ratioHeight)
        maxHeight = image.height()*ratioWidth;


    ui->imageLabel->setGeometry(ui->imageLabel->geometry().x(),
                                ui->imageLabel->geometry().y(),
                                maxWidth,maxHeight);
    QPixmap pixmap = QPixmap::fromImage(image);

    ui->imageLabel->setPixmap(pixmap.scaled(maxWidth,maxHeight));


    ui->drawingLabel->setGeometry(ui->imageLabel->geometry().x(),
                                ui->imageLabel->geometry().y(),
                                maxWidth,maxHeight);
    QString maskName = getMaskDir()+fileName;

    if(QFile(maskName).exists())
    {
        //Convert B/W Mask to Transparent Overlay
        QImage img(maskName);
        img = img.scaled(maxWidth,maxHeight);

        QImage tmpImage(img.width(),img.height(),QImage::Format_ARGB32);
        tmpImage.fill(QColor("transparent"));
        for(int x=0; x < img.width();x++)
        {
            for(int y =0; y< img.height();y++)
            {
                if(img.pixel(x,y)&0xFFFFFF)//Ignore Alpha Channel
                {
                    tmpImage.setPixel(x,y,0xB2FF0000);//70% opaque Red
                }
            }
        }
        ui->drawingLabel->setupPainter(QPixmap::fromImage(tmpImage));
    }
    else
        ui->drawingLabel->setupPainter();


}
void MainWindow::nextImage()
{

    if(imagesIt != nullptr &&imagesIt+1 != images.constEnd())
    {
        ui->drawingLabel->clearMask();
        ++imagesIt;
        loadImage(*imagesIt);
        currentImageNo++;
        setCurrentImage();
    }
}
void MainWindow::prevImage()
{
    if(imagesIt != nullptr && imagesIt != images.constBegin())
    {
        ui->drawingLabel->clearMask();
        --imagesIt;
        loadImage(*imagesIt);
        currentImageNo--;
        setCurrentImage();
    }

}
void MainWindow::saveImage()
{
    QString maskDirName = getMaskDir();
    if(!QDir(maskDirName).exists())
        QDir().mkdir(maskDirName);

    if(ui->drawingLabel->savePixmap(maskDirName+(*imagesIt),originalImageSize))
    {
        QMessageBox msgBox;
        msgBox.setText("Image successfull saved");
        msgBox.exec();
    }
}
void MainWindow::clearMask()
{
    ui->drawingLabel->clearMask();
}
void MainWindow::eraseDrawToggle()
{
   isErasing =!isErasing;
   if(isErasing)
       ui->eraseDrawButton->setText("draw");
   else
       ui->eraseDrawButton->setText("erase");

   ui->drawingLabel->changeDrawMode(isErasing);

}
QString MainWindow::buildImagePath(QString fileName)
{
    return currentDir+"/"+fileName;
}
QString MainWindow::getMaskDir()
{
    return currentDir+"/masks/";
}
void MainWindow::jumpTo()
{
    QDir recoredDir(getMaskDir());
    QStringList filters;
    filters << "*.png" << "*.jpg" <<"*.JPG";
    recoredDir.setNameFilters(filters);
    QStringList maskImages = recoredDir.entryList(QDir::NoDotAndDotDot | QDir::Filter::Files);
    loadImage(maskImages.last());

    imagesIt = images.constBegin();
    currentImageNo = 1;
    while(imagesIt != images.constEnd())
    {
        ++imagesIt;
        ++currentImageNo;
        if((*imagesIt) == maskImages.last())
            break;
    }
    setCurrentImage();

}

void MainWindow::setCurrentImage()
{
    ui->currentImage->setText(QString::number(currentImageNo)+"/"+QString::number(totalImages));

}

MainWindow::~MainWindow()
{
    delete ui;
}
