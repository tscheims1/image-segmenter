#-------------------------------------------------
#
# Project created by QtCreator 2016-08-04T14:25:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ImageSegmenter
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += main.cpp\
        mainwindow.cpp \
    clickablelabel.cpp

HEADERS  += mainwindow.h \
    clickablelabel.h

FORMS    += mainwindow.ui
